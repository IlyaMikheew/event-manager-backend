package com.example.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="comment_likes")
public class CommentLikes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "comment_id")
    private long commentId;

}
