package com.example.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name="comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="event_id")
    private long eventId;

    @Column(name="user_id")
    private long userId;

    private String text;

    private int likes;

    @Column(name = "add_time",
    columnDefinition = "add_time timestamp default CURRENT_TIMESTAMP",
    updatable = false)
    private LocalDateTime addTime;

}
