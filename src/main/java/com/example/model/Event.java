package com.example.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "event")
@Data
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="address")
    private Long address;

    @Column(name= "description")
    private String description;

    @Column(name="author")
    private long author;

    @Column(name="finished")
    private boolean finished;

    @Column(name="start_date")
    private LocalDateTime startDate;

    @Column(name="end_date")
    private LocalDateTime endDate;

    @Column(name = "image")
    private String image;

    @ManyToOne
    @JoinColumn(name = "address", referencedColumnName = "id", insertable = false, updatable = false)
    private Location location;

}
