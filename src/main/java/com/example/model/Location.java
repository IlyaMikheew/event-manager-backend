package com.example.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="location")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String address;

    @Column(name = "x_coords")
    private double xCords;

    @Column(name = "y_coords")
    private double yCords;

    @OneToMany(mappedBy = "location")
    private List<Event> events;

    @JsonIgnore
    public List<Event> getEvents() {
        return this.events;
    }
}
