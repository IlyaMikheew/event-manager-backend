package com.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Table(name="user", schema="public")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="fname")
    private String fName;

    @Column(name="lname")
    private String lName;

    private String gender;

    private String email;

    private String phone;

    private String password;

    @Column(name="reg_date")
    private LocalDateTime regDate;

    private int age;

    @Column(name="events_created")
    private int eventsCreated;

    private String description;

    @Column(name = "avatar")
    private String avatar;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_roles",
    joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @JsonIgnore
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
