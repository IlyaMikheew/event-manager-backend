package com.example.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Table(name="timeline_item")
@Entity
public class TimelineItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="event_id")
    private long eventId;

    private LocalDateTime time;

    @Column(name= "description")
    private String description;

}
