package com.example.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="subscription")
@Data
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "user_id")
    private long userId;

    private  long event;
}
