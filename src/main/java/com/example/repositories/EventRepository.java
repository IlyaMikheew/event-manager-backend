package com.example.repositories;

import com.example.dto.EventListDto;
import com.example.model.Event;
import com.example.model.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long > {

    @Query(value = "select new com.example.dto.EventListDto(e.id, e.name, e.startDate, l.address, e.author)\n" +
            "from Event e\n" +
            "join Subscription s on e.id = s.event\n" +
            "join User u on s.userId = u.id\n" +
            "join Location l on e.address = l.id\n"+
            "where u.email = ?1")
    Page<EventListDto> findSubscribedEventsByUser(String email, Pageable pageable);

    @Query(value = "select e\n" +
            "from Event e\n" +
            "join Subscription s on e.id = s.event\n" +
            "join User u on s.userId = u.id\n" +
            "join Location l on e.address = l.id\n"+
            "where u.email = ?1")
    List<Event> findSubscribedEventsByUser(String email);

    @Query(value = "select new com.example.dto.EventListDto(e.id, e.name, e.startDate, l.address, e.author)\n" +
            "from Event e\n" +
            "join User u on e.author = u.id\n" +
            "join Location l on e.address = l.id\n" +
            "where u.email = ?1")
    Page<EventListDto> findEventsByAuthor(String email, Pageable pageable);

    @Query(value = "select new com.example.dto.EventListDto(e.id, e.name, e.startDate, l.address, e.author)\n" +
            "from Event e\n" +
            "join Location l on e.address = l.id\n"+
            "where e.finished = true")
    Page<EventListDto> findEventByFinished(Boolean finished, Pageable pageable);

    @Query(value = "select new com.example.dto.EventListDto(e.id, e.name, e.startDate, l.address, e.author)\n" +
            "from Event e\n" +
            "join Location l on e.address = l.id\n")
    Page<EventListDto> findAllWithAddress (Pageable pageable);

    @Query(value = "select e from Event e\n " +
            "where e.name like %?1%")
    List<Event> searchByName (String namePart);

    Event findEventById(Long id);

    Event findTopByOrderByIdDesc();

    @Query(value = "select e.id from Event e\n" +
            "join Subscription s on e.id = s.event \n " +
            "join User u on u.id = s.userId \n " +
            "where u.email = ?1")
    List<Long> findSubscribedEventsIdByUser(String userEmail);
}
