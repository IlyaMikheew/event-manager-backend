package com.example.repositories;

import com.example.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    Subscription findSubscriptionByEventAndUserId(long eventId, long userId);
}
