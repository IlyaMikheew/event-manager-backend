package com.example.repositories;

import com.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    @Query(value = "select u from User u\n" +
            "join Subscription s on s.userId=u.id\n" +
            "join Event e on e.id=s.event where e.id = ?1")
    List<User> findSubscribedUsersByEventId(Long eventId);

}
