package com.example.repositories;

import com.example.dto.CommentDto;
import com.example.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "select new com.example.dto.CommentDto(c.id, c.text, c.likes, c.addTime, u.fName, u.lName, u.avatar)\n" +
            "from Comment c join User u on c.userId = u.id\n" +
            "where c.eventId = ?1")
    Page<CommentDto> findCommentsByEventId(long id, Pageable pageable);

    Comment getCommentById(Long id);

}
