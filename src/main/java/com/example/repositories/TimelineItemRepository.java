package com.example.repositories;

import com.example.model.TimelineItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimelineItemRepository extends JpaRepository<TimelineItem, Long> {
    List<TimelineItem> findTimelineItemsByEventId(Long id);
}
