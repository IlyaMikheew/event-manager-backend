package com.example.repositories;

import com.example.model.CommentLikes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentLikesRepository extends JpaRepository<CommentLikes, Long> {

    CommentLikes findByUserIdAndCommentId(Long userId, Long commentId);

    @Query(value = "select cl.commentId\n" +
            "from CommentLikes cl join Comment c on cl.commentId = c.id\n" +
            "where cl.userId = ?1 and c.eventId = ?2")
    List<Long> findLikedCommentsByUserId(Long userId, long eventId);

}
