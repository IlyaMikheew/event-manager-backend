package com.example.controller;

import com.example.dto.LoginDto;
import com.example.dto.RegistrationDto;
import com.example.model.User;
import com.example.security.jwt.JwtTokenProvider;
import com.example.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class AuthorizationController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;


    @PostMapping("/registration")
    @ResponseBody
    public ResponseEntity<String> createUser (@Valid @RequestBody RegistrationDto regUser,
                                                  BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return ResponseEntity.
                    status(HttpStatus.BAD_REQUEST)
                    .body("User no valid");
        }
        return userService.createUser(regUser);
    }

    @PostMapping("/login")
    public ResponseEntity< Map<Object, Object>> login (@RequestBody LoginDto loginUser) {
        try {
            String username = loginUser.getEmail();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginUser.getPassword()));
            User user = userService.findByEmail(username);
            if(user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }
            String token = jwtTokenProvider.createToken(username, user.getRoles());
            Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);
            response.put("userId", user.getId());
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
