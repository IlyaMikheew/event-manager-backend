package com.example.controller;

import com.example.dto.CommentDto;
import com.example.model.Comment;
import com.example.repositories.CommentRepository;
import com.example.services.CommentService;
import com.example.services.impl.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CommentController {

    @Autowired
    private CommentServiceImpl commentService;

    @GetMapping("/comments")
    public Map<Object, Object> getEventComments(@RequestParam Long eventId,
                                                @RequestParam String userEmail,
                                             @RequestParam int start,
                                             @RequestParam int limit,
                                             @RequestParam int page){
        return commentService.getCommentsByEventId(eventId, userEmail, limit, page);
    }

    @PostMapping("/comments")
    public Page<CommentDto> createComment(@RequestBody Comment comment){
        return commentService.createComment(comment);
    }

    @PostMapping("/comments/likes")
    public Map<Object, Object> setLike(@RequestParam long commentId,
                                             @RequestParam String userEmail,
                                             @RequestParam int page){
        return commentService.setLike(commentId, userEmail, page);
    }

    @DeleteMapping("/comments/likes")
    public Map<Object, Object> deleteLike(@RequestParam long commentId,
                                    @RequestParam String userEmail,
                                    @RequestParam int page){
        return commentService.deleteLike(commentId, userEmail, page);
    }
}
