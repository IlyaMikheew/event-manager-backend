package com.example.controller;

import com.example.dto.EditUserDto;
import com.example.dto.EventListDto;
import com.example.model.Event;
import com.example.model.User;
import com.example.services.impl.EventServiceImpl;
import com.example.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000/", allowedHeaders = "*")
public class UserController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private EventServiceImpl eventService;

    @GetMapping("/users")
    public List<User> getAllUser (){
        return userService.getAll();
    }


    @PostMapping("/users/{id}")
    public ResponseEntity<Map<Object, Object>> findContactById(
            @PathVariable(value = "id") Long userId, @RequestBody Map<Object, Object> pagination) throws ResourceNotFoundException {
        User user = userService.getById(userId);
        Map<Object, Object> result = new HashMap<>();
        if(pagination.size() == 0) {
            result.put("user", user);
            return ResponseEntity.ok().body(result);
        }
        int page = (int) pagination.get("page") - 1;
        int limit = (int) pagination.get("limit");
        Page<EventListDto> userEvents = eventService.getEventsUserSubscribedTo(user.getEmail(), page, limit);
        result.put("user", user);
        result.put("userEvents", userEvents);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/users/edit")
    public ResponseEntity<String> editUser (@RequestBody EditUserDto editedUser) {
        return userService.editUser(editedUser);
    }
}
