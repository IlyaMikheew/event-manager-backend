package com.example.controller;

import com.example.dto.AddEventDto;
import com.example.dto.EditEventDto;
import com.example.dto.EventListDto;
import com.example.model.Event;
import com.example.services.impl.EventServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EventController {

    @Autowired
    private EventServiceImpl eventService;

    @GetMapping("/events")
    public Map<Object, Object> getAllEvents (@RequestParam String userEmail) {
        List<Map<Object, Object>> events = eventService.getAllEventLocation();
        List<Long> subscribedEvents = eventService.getEventsIdUserSubscribedTo(userEmail);
        Map<Object, Object> result = new HashMap<>();
        result.put("events", events);
        result.put("subscribedEvents", subscribedEvents);
        return result;
    }

    @PostMapping("/events")
    public Map<Object, Object> getEvents (@RequestBody HashMap<String, HashMap<String,String>> data,
                                         @RequestParam String userEmail) {
        String property = data.get("filter").get("property");
        String value = data.get("filter").get("value");
        HashMap<String,String> pagination = data.get("pagination");
        HashMap<String,String> sort = data.get("sort");
        Page<EventListDto> page = eventService.getFilteredEvents(value, pagination, sort, userEmail);
        List<Event> events = eventService.getEventsUserSubscribedTo(userEmail);
        Map<Object, Object> result = new HashMap<>();
        result.put("page", page);
        result.put("subscribedEvents", events);
        return result;
    }

    @GetMapping("/events/{id}")
    public ResponseEntity<Map<Object, Object>> getEvent( @PathVariable(value = "id") Long eventId ){
        return eventService.getEventInfoById(eventId);
    }

    @PostMapping("/events/subscribe")
    public ResponseEntity<Map<Object, Object>> subscribe( @RequestParam long eventId, @RequestParam String userEmail){
        return eventService.subscribeToEvent(eventId, userEmail);
    }

    @DeleteMapping("/events/subscribe")
    public ResponseEntity<Map<Object, Object>> unsubscribe( @RequestParam long eventId, @RequestParam String userEmail){
        return eventService.unsubscribeFromEvent(eventId, userEmail);
    }

    @PostMapping("/events/create")
    public ResponseEntity createEvent (@RequestBody AddEventDto event, @RequestParam String userEmail) {
        return eventService.createEvent(event, userEmail);
    }

    @GetMapping("/events/search")
    public List<Event> searchEvent (@RequestParam String namePart){
        return eventService.findEvent(namePart);
    }

    @PostMapping("/events/edit")
    public ResponseEntity editEvent (@RequestBody EditEventDto event, @RequestParam String userEmail) {
        return eventService.editEvent(event, userEmail);
    }
}
