package com.example.services.impl;

import com.example.dto.CommentDto;
import com.example.model.Comment;
import com.example.model.CommentLikes;
import com.example.model.Event;
import com.example.model.User;
import com.example.repositories.CommentLikesRepository;
import com.example.repositories.CommentRepository;
import com.example.repositories.EventRepository;
import com.example.repositories.UserRepository;
import com.example.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentLikesRepository commentLikesRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private SimpMessagingTemplate template;

    @Override
    public List<Comment> getAll() {
        return commentRepository.findAll();
    }

    public Map<Object, Object> getCommentsByEventId(Long eventId, String userEmail, int limit, int page) {
        Pageable pageRequest = PageRequest.of(page-1, limit, Sort.by("id").descending());
        Map<Object, Object> response = new HashMap<>();
        User user = userRepository.findByEmail(userEmail);
        response.put("page", commentRepository.findCommentsByEventId(eventId, pageRequest));
        response.put("likedComments", commentLikesRepository.findLikedCommentsByUserId(user.getId(), eventId));
        return response;
    }

    public Page<CommentDto> createComment(Comment comment) {
        List<User> subscribers = userRepository.findSubscribedUsersByEventId(comment.getEventId());
        Event event = eventRepository.findEventById(comment.getEventId());
        String dest = "";
        String payload = "New comment on event " + event.getName();
        for(User user : subscribers) {
            if (user.getId() == comment.getUserId()){
                continue;
            }
            dest = "/topic/notifications/" + user.getId();
            template.convertAndSend(dest, payload);
        }
        comment.setAddTime(LocalDateTime.now());
        commentRepository.save(comment);
        Pageable pageRequest = PageRequest.of(0, 2, Sort.by("id").descending());
        return commentRepository.findCommentsByEventId(comment.getEventId(), pageRequest);
    }

    public Map<Object, Object> setLike(long commentId, String userEmail, int page) {
        Comment comment = commentRepository.getCommentById(commentId);
        User user = userRepository.findByEmail(userEmail);
        CommentLikes userLike = commentLikesRepository.findByUserIdAndCommentId(user.getId(), commentId);
        if (userLike == null) {
            CommentLikes newLike = new CommentLikes();
            newLike.setCommentId(comment.getId());
            newLike.setUserId(user.getId());
            commentLikesRepository.save(newLike);

            int likes = comment.getLikes();
            comment.setLikes(++likes);
            commentRepository.save(comment);
        }
        return getCommentsByEventId(comment.getEventId(), user.getEmail(), 2, page);
    }

    public Map<Object, Object> deleteLike(long commentId, String userEmail, int page) {
        Comment comment = commentRepository.getCommentById(commentId);
        User user = userRepository.findByEmail(userEmail);
        CommentLikes like = commentLikesRepository.findByUserIdAndCommentId(user.getId(), commentId);
        commentLikesRepository.delete(like);
        int likes = comment.getLikes();
        comment.setLikes(--likes);
        commentRepository.save(comment);
        return getCommentsByEventId(comment.getEventId(), user.getEmail(), 2, page);
    }
}
