package com.example.services.impl;

import com.example.dto.EditUserDto;
import com.example.dto.LoginDto;
import com.example.dto.RegistrationDto;
import com.example.model.Event;
import com.example.model.Role;
import com.example.model.Status;
import com.example.model.User;
import com.example.repositories.RoleRepository;
import com.example.repositories.UserRepository;
import com.example.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));
    }

    @Override
    public ResponseEntity createUser(RegistrationDto regUser) {
        if(userRepository.findByEmail(regUser.getEmail()) == null){
            try{
                User user = modelMapper.map(regUser, User.class);
                user.setPassword(passwordEncoder.encode(regUser.getPassword()));
                user.setStatus(Status.ACTIVE);
//            Role role = new Role();
//            role.setName("ROLE_USER");
//            List<Role> roles = new ArrayList<>();
//            roles.add(role);
//            user.setRoles(roles);
                userRepository.save(user);
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body("User created");
            } catch(Exception e) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body("Creation user error");
            }
        }
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body("User with such email already exists");
    }

    public ResponseEntity<HttpStatus> login (LoginDto loginUser) {
        User user = userRepository.findByEmail(loginUser.getEmail());
        if(user == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(user.getPassword().equals(loginUser.getPassword())) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Override
    public User findByEmail (String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void deleteUser(Long id) {
    }

    public List<Event> getUserEventsById (Long userId) {
        return null;
    }

    public ResponseEntity<String> editUser (EditUserDto editedUser) {
        try {
            User user = userRepository.getById(editedUser.getId());
            user.setEmail(editedUser.getEmail());
            user.setFName(editedUser.getFName());
            user.setLName(editedUser.getLName());
            user.setDescription(editedUser.getDescription());
            user.setPhone(editedUser.getPhone());
            user.setAvatar(editedUser.getAvatar());
            userRepository.save(user);
            return ResponseEntity.ok("User edited correctly");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unable to edit user");
        }
    }
}
