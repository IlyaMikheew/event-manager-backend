package com.example.services.impl;

import com.example.dto.AddEventDto;
import com.example.dto.EditEventDto;
import com.example.dto.EventListDto;
import com.example.model.*;
import com.example.repositories.*;
import com.example.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    private final CommentRepository commentRepository;
    private final TimelineItemRepository timelineItemRepository;
    private final UserRepository userRepository;
    private final SubscriptionRepository subscriptionRepository;
    private final LocationRepository locationRepository;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository, CommentRepository commentRepository,
                            TimelineItemRepository timelineItemRepository,
                            UserRepository userRepository, SubscriptionRepository subscriptionRepository,
                            LocationRepository locationRepository) {
        this.eventRepository = eventRepository;
        this.commentRepository = commentRepository;
        this.timelineItemRepository = timelineItemRepository;
        this.userRepository = userRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.locationRepository = locationRepository;
    }

    @Override
    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    public Page<EventListDto> getFilteredEvents(String value,
                                         HashMap<String, String> pagination,
                                         HashMap<String, String> sort,
                                                String userEmail) {
        int page = Integer.parseInt(pagination.get("page")) - 1;
        int size = Integer.parseInt(pagination.get("limit"));
        String property = sort.get("property");
        String direction = sort.get("direction");
        Pageable pageRequest = direction.equals("ascend") ? PageRequest.of(page,size, Sort.by(property).ascending()) :
                PageRequest.of(page,size, Sort.by(property).descending());
        Page<EventListDto> result;
        switch (value){
            case ("all"):
                result = eventRepository.findAllWithAddress(pageRequest);
                break;
            case ("favorites"):
                result = eventRepository.findSubscribedEventsByUser(userEmail, pageRequest);
                break;
            case ("my"):
                result = eventRepository.findEventsByAuthor(userEmail, pageRequest);
                break;
            case ("finished"):
                result = eventRepository.findEventByFinished(true, pageRequest);
                break;
            default:
                result = eventRepository.findAllWithAddress(pageRequest);
        }
        if (!"finished".equals(value)){
            for(EventListDto e : result){
                Event event = eventRepository.findEventById(e.getId());
                if(event.getEndDate().isBefore(LocalDateTime.now()) && !event.isFinished()){
                    event.setFinished(true);
                    eventRepository.save(event);
                }
            };
        }

        return result;
    }

    public Event getById(long eventId){
        return eventRepository.findById(eventId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));
    }

    public ResponseEntity getEventInfoById (Long eventId) {
        Map response = new HashMap<String, Object>();
        response.put("event", eventRepository.findEventById(eventId));
        response.put("timelineItems", timelineItemRepository.findTimelineItemsByEventId(eventId));
        response.put("users", userRepository.findSubscribedUsersByEventId(eventId));
        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<Map<Object, Object>> subscribeToEvent(long eventId, String userEmail) {
        Map<Object, Object> response = new HashMap<>();
        User user = userRepository.findByEmail(userEmail);
        Subscription subscription = subscriptionRepository.findSubscriptionByEventAndUserId(eventId, user.getId());
        if (subscription == null){
            subscription = new Subscription();
            subscription.setEvent(eventId);
            subscription.setUserId(user.getId());
            subscriptionRepository.save(subscription);
        }
        response.put("users", userRepository.findSubscribedUsersByEventId(eventId));
        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity<Map<Object, Object>> unsubscribeFromEvent(long eventId, String userEmail) {
        Map<Object, Object> response = new HashMap<>();
        User user = userRepository.findByEmail(userEmail);
        Subscription s = subscriptionRepository.findSubscriptionByEventAndUserId(eventId, user.getId());
        subscriptionRepository.delete(s);
        response.put("users", userRepository.findSubscribedUsersByEventId(eventId));
        return ResponseEntity.ok().body(response);
    }

    public ResponseEntity createEvent(AddEventDto event, String userEmail) {
        ZoneOffset offset = ZoneOffset.ofHours(3);
        User user = userRepository.findByEmail(userEmail);
        Event newEvent = new Event();
        Location newLocation = new Location();

        newLocation.setAddress((String) event.getLocation().get("address"));
        List<Double> coordinates = (List<Double>) event.getLocation().get("coordinates");
        newLocation.setXCords(coordinates.get(0));
        newLocation.setYCords(coordinates.get(1));

        locationRepository.save(newLocation);
        newLocation = locationRepository.findTopByOrderByIdDesc();
        newEvent.setName(event.getName());
        newEvent.setDescription(event.getDescription());
        newEvent.setStartDate(LocalDateTime.ofEpochSecond(event.getStartDate(), 0, offset));
        newEvent.setEndDate(LocalDateTime.ofEpochSecond(event.getEndDate(), 0, offset));
        newEvent.setAuthor(user.getId());
        newEvent.setAddress(newLocation.getId());
        newEvent.setImage(event.getImage());
        newEvent.setFinished(false);
        eventRepository.save(newEvent);
        Long newEventId = eventRepository.findTopByOrderByIdDesc().getId();

        List<Map<Object, Object>> timelineItems = event.getTimelineItems();
        for (Map item : timelineItems) {
            TimelineItem newTimelineItem = new TimelineItem();
            newTimelineItem.setEventId(newEventId);
            newTimelineItem.setDescription((String) item.get("desc"));
            Long timestamp = Long.valueOf( (int) item.get("time"));
            newTimelineItem.setTime(LocalDateTime.ofEpochSecond(timestamp, 0, offset));
            timelineItemRepository.save(newTimelineItem);
        }

        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    public List<Event> findEvent (String namePart) {
        return eventRepository.searchByName(namePart);
    }

    public ResponseEntity editEvent(EditEventDto event, String userEmail) {
        ZoneOffset offset = ZoneOffset.ofHours(3);
        Event editedEvent = eventRepository.findEventById(event.getId());
        editedEvent.setName(event.getName());
        editedEvent.setDescription(event.getDescription());
        editedEvent.setImage(event.getImage());
        editedEvent.setAuthor(event.getAuthor());
        editedEvent.setFinished(false);
        editedEvent.setStartDate(LocalDateTime.ofEpochSecond(event.getStartDate(), 0, offset));
        editedEvent.setEndDate(LocalDateTime.ofEpochSecond(event.getEndDate(), 0, offset));

        Location newLocation = new Location();
        newLocation.setAddress((String) event.getLocation().get("address"));
        List<Double> coordinates = (List<Double>) event.getLocation().get("coordinates");
        newLocation.setXCords(coordinates.get(0));
        newLocation.setYCords(coordinates.get(1));
        locationRepository.save(newLocation);
        newLocation = locationRepository.findTopByOrderByIdDesc();

        editedEvent.setAddress(newLocation.getId());

        eventRepository.save(editedEvent);

        List<User> subscribers = userRepository.findSubscribedUsersByEventId(editedEvent.getId());
        String dest = "";
        String payload = "Event \"" + editedEvent.getName() + "\" has been changed";
        for(User user : subscribers) {
            if (user.getEmail().equals(userEmail)){
                continue;
            }
            dest = "/topic/notifications/" + user.getId();
            template.convertAndSend(dest, payload);
        }

        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    public List<Event> getEventsUserSubscribedTo (String userEmail) {
        return eventRepository.findSubscribedEventsByUser(userEmail);
    }

    public List<Long> getEventsIdUserSubscribedTo (String userEmail) {
        List<Event> events = eventRepository.findSubscribedEventsByUser(userEmail);
        List<Long> result = new ArrayList<>();
        for (Event e : events) {
            result.add(e.getId());
        }
        return result;
    }

    public Page<EventListDto> getEventsUserSubscribedTo (String userEmail, int page, int limit) {
        Pageable pageRequest = PageRequest.of(page, limit);
        return eventRepository.findSubscribedEventsByUser(userEmail, pageRequest);
    }

    public List<Map<Object, Object>> getAllEventLocation () {
        List<Map<Object, Object>> result = new ArrayList<>();
        List<Event> events = getAll();
        for(Event e : events) {
            Map<Object, Object> el = new HashMap<>();
            el.put("location", locationRepository.findById(e.getAddress()));
            el.put("eventId", e.getId());
            result.add(el);
        }
        return result;
    }

}
