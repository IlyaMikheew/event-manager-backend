package com.example.services;

import com.example.dto.AddEventDto;
import com.example.dto.EditEventDto;
import com.example.dto.EventListDto;
import com.example.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface EventService {
    List<Event> getAll();
    Page<EventListDto> getFilteredEvents(String value,
                                                HashMap<String, String> pagination,
                                                HashMap<String, String> sort,
                                                String userEmail);
    Event getById(long eventId);
    ResponseEntity<Map<Object, Object>> subscribeToEvent(long eventId, String userEmail);
    ResponseEntity<Map<Object, Object>> unsubscribeFromEvent(long eventId, String userEmail);
    ResponseEntity createEvent(AddEventDto event, String userEmail);
    List<Event> findEvent (String namePart);
    ResponseEntity editEvent(EditEventDto event, String userEmail);
    List<Event> getEventsUserSubscribedTo (String userEmail);
    List<Long> getEventsIdUserSubscribedTo (String userEmail);
    Page<EventListDto> getEventsUserSubscribedTo (String userEmail, int page, int limit);
    List<Map<Object, Object>> getAllEventLocation ();
}
