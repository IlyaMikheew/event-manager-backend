package com.example.services;

import com.example.dto.RegistrationDto;
import com.example.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User getById(Long id);
    ResponseEntity<HttpStatus> createUser(RegistrationDto user);
    void deleteUser(Long id);
    User findByEmail(String email);

}
