package com.example.dto;

import lombok.Data;

@Data
public class EditUserDto {
    private Long id;
    private String avatar;
    private String email;
    private String fName;
    private String lName;
    private String description;
    private String phone;

    public EditUserDto(Long id, String email, String fName, String lName, String phone) {
        this.id = id;
        this.email = email;
        this.fName = fName;
        this.lName = lName;
        this.phone = phone;
    }
}
