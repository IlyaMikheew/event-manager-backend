package com.example.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class EditEventDto {
    private Long id;
    private Long author;
    private String image;
    private String name;
    private String description;
    private Long startDate;
    private Long endDate;
    private Map<Object, Object> location;

    public EditEventDto(Long id, String image, String name, String description, Long startDate, Long endDate, Map<Object, Object> location) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
    }
}
