package com.example.dto;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class EventListDto {
    private long id;
    private String name;
    private LocalDateTime startDate;
    private String address;
    private Long author;

    public EventListDto(long id, String name, LocalDateTime startDate, String address, Long author) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.address = address;
        this.author = author;
    }
}
