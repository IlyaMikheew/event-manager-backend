package com.example.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommentDto {
    private long id;
    private String text;
    private int likes;
    private LocalDateTime addTime;

    private String fName;
    private String lName;
    private String avatar;

    public CommentDto(long id, String text, int likes, LocalDateTime addTime, String fName, String lName, String avatar) {
        this.id = id;
        this.text = text;
        this.likes = likes;
        this.addTime = addTime;
        this.fName = fName;
        this.lName = lName;
        this.avatar = avatar;
    }
}
