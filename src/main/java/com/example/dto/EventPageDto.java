package com.example.dto;

import com.example.model.Event;
import com.example.model.User;
import lombok.Data;

import java.util.List;

@Data
public class EventPageDto {
    private Event event;
    private List<User> users;

    public EventPageDto(Event event, List<User> users) {
        this.event = event;
        this.users = users;
    }
}
