package com.example.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
public class RegistrationDto {
    private Long id;

    @NotEmpty
    private String fName;

    @NotEmpty
    private String lName;

    @NotEmpty
    private String gender;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String phone;

    @NotEmpty
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$")
    private String password;

    @NotNull
    private LocalDateTime regDate;

    @Min(value = 0)
    @Max(120)
    private int age;

    private int eventsCreated;

    @NotNull
    private String description;

}
