package com.example.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class AddEventDto {
    private String image;
    private String name;
    private String description;
    private Long startDate;
    private Long endDate;
    private Map<Object, Object> location;
    private List<Map<Object, Object>> timelineItems;

    public AddEventDto(String image, String name, String description, Long startDate, Long endDate, Map<Object, Object> location, List<Map<Object, Object>> timelineItems) {
        this.image = image;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.timelineItems = timelineItems;
    }
}
